﻿using LzSoftware.Phonetics.Soundex;
using NUnit.Framework;

namespace LzSoftware.Phonetics.Tests.Soundex
{
    /// <summary>
    /// A class which contains unit tests for <see cref="T:LzSoftware.Phonetics.Soundex.SoundexAlgorithm"/>.
    /// </summary>
    [TestFixture]
    public sealed class SoundexAlgorithmTests
    {
        /// <summary>
        /// Checks that <see cref="M:LzSoftware.Phonetics.Soundex.SoundexAlgorithm.CalculateSoundexKey(System.String)"/> calculates the correct values.
        /// </summary>
        /// <param name="input">The input <see cref="T:System.String"/>.</param>
        /// <returns>The generated soundex key.</returns>
        [Test]
        [TestCase("Robert", Result = "R163")]
        [TestCase("Rupert", Result = "R163")]
        [TestCase("Rubin", Result = "R150")]
        [TestCase("Ashcraft", Result = "A261")]
        [TestCase("Ashcroft", Result = "A261")]
        [TestCase("Pfister", Result = "P236")]
        [TestCase("Smythe", Result = "S530")]
        [TestCase("Smith", Result = "S530")]
        [TestCase("5P", Result = "0000")]
        [TestCase("Shine", Result = "S500")]
        [TestCase("Utpb", Result = "U310")]
        [TestCase("Tymczak", Result = "T522")]
        public string CalculateSoundexKeyTest(string input)
        {
            // Act
            return SoundexAlgorithm.CalculateSoundexKey(input);
        }
    }
}