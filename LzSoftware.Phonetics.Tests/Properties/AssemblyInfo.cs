﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("LzSoftware.phonetics.Tests")]
[assembly: AssemblyDescription("An assembly which contains the unit tests for the phonetics assembly")]
[assembly: ComVisible(false)]
[assembly: Guid("8cd4f979-23d4-4d57-8bed-8c7a65bdb203")]