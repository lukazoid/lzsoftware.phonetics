@echo off
for /D %%D in (%SYSTEMROOT%\Microsoft.NET\Framework\v4*) do set msbuild=%%D\MSBuild.exe
set Configuration=Release
%msbuild% default.msbuild /v:n /t:RunUnitTests
pause