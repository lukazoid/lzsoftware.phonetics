﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

[assembly: AssemblyTitle("LzSoftware.phonetics")]
[assembly: AssemblyDescription("An assembly which contains functionality for the phonetics of strings")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]
[assembly: InternalsVisibleTo("LzSoftware.Phonetics.Tests,PublicKey=0024000004800000940000000602000000240000525341310004000001000100d96454a09710baed45f0f9ca9f2bd8c03953051756f6770b20ca3dea13156e0103b3b0e990b2818306900fc3fad0a901c7809e566af562d6d7c5665429e0765a35dee9324de3f18d9df2601830c6e845da9fce61653a469fe163bc2a0543dc229a0e944b489045572b3ee47f5f8b9af0a97a514ce8a083489803e10d37f401ed")]

#if !NCRUNCH
[assembly: AllowPartiallyTrustedCallers]
#endif
