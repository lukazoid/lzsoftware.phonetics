﻿using System;
using System.Diagnostics.Contracts;
using LzSoftware.Phonetics.Soundex;

namespace LzSoftware.Phonetics
{
    /// <summary>
    /// A class which contains the phonetic <see cref="T:System.StringComparer"/> implementation.
    /// </summary>
    public static class PhoneticComparer
    {
        private static readonly StringComparer SoundexComparerSingleton = new SoundexEqualityComparer();

        /// <summary>
        /// Gets the soundex <see cref="T:System.StringComparer"/>.
        /// </summary>
        public static StringComparer Soundex
        {
            get
            {
                Contract.Ensures(Contract.Result<StringComparer>() != null);

                return SoundexComparerSingleton;
            }
        }
    }
}