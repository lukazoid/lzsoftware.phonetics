﻿using System;
using System.Diagnostics.Contracts;

namespace LzSoftware.Phonetics.Soundex
{
    internal sealed class SoundexEqualityComparer : StringComparer
    {
        private static readonly StringComparer KeyComparer = OrdinalIgnoreCase;

        #region Overrides of StringComparer

        public override int Compare(string x, string y)
        {
            if (ReferenceEquals(x, y))
                return 0;

            if (x == null)
                return -1;
            if (y == null)
                return 1;

            var xSoundexKey = CalculateSoundexKey(x);
            var ySoundexKey = CalculateSoundexKey(y);

            return KeyComparer.Compare(xSoundexKey, ySoundexKey);
        }

        public override bool Equals(string x, string y)
        {
            if (ReferenceEquals(x, y))
                return true;

            if (x == null || y == null)
                return false;

            var xSoundexKey = CalculateSoundexKey(x);
            var ySoundexKey = CalculateSoundexKey(y);

            return KeyComparer.Equals(xSoundexKey, ySoundexKey);
        }

        public override int GetHashCode(string obj)
        {
            if (obj == null)
                return 0;

            var soundexKey = CalculateSoundexKey(obj);

            return KeyComparer.GetHashCode(soundexKey);
        }

        private static string CalculateSoundexKey(string obj)
        {
            Contract.Requires(obj != null);

            return SoundexAlgorithm.CalculateSoundexKey(obj);
        }

        #endregion
    }
}