﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;

namespace LzSoftware.Phonetics.Soundex
{
    /// <summary>
    /// An implementation of the soundex algorithm. 
    /// </summary>
    public static class SoundexAlgorithm
    {
        /// <summary>
        /// Calculates the soundex key of the specified <see cref="T:System.String"/>.
        /// </summary>
        /// <param name="input">The input <see cref="T:System.String"/>.</param>
        /// <returns>The four character soundex key.</returns>
        public static string CalculateSoundexKey(string input)
        {
            Contract.Requires(input != null);
            Contract.Ensures(Contract.Result<string>() != null);
            Contract.Ensures(Contract.Result<string>().Length == 4);

            var builder = new SoundexBuilder(input);

            return builder.BuildSoundex();
        }

        #region Nested type: SoundexBuilder

        private sealed class SoundexBuilder
        {
            private readonly string _input;
            private readonly int _inputLength;

            private int _index = 1;

            public SoundexBuilder(string input)
            {
                Contract.Requires(input != null);

                _input = input;
                _inputLength = _input.Length;
            }

            public string BuildSoundex()
            {
                Contract.Ensures(Contract.Result<string>() != null);
                Contract.Ensures(Contract.Result<string>().Length == 4);

                if (_inputLength == 0)
                    return "0000";

                var firstChar = _input[0];
                var firstCharUpper = Char.ToUpperInvariant(firstChar);

                var isRecognizedAlphabeticCharacter = firstCharUpper >= 'A' && firstCharUpper <= 'Z';
                if (!isRecognizedAlphabeticCharacter)
                    return "0000";

                // Always starts with the first character, no matter what it is
                var soundexKey = new string(firstCharUpper, 1);

                var firstNumericKey = TryGetNumericKey(firstCharUpper);

                var secondNumericKey = MoveToFirstRealCharacterOfInterest(firstNumericKey);
                if (!secondNumericKey.HasValue)
                    return soundexKey + "000";

                var secondNumericKeyValue = secondNumericKey.Value;
                soundexKey += ToString(secondNumericKeyValue);
                ++_index;

                var thirdNumericKey = MoveToNextCharacterOfInterest(secondNumericKeyValue);
                if (!thirdNumericKey.HasValue)
                    return soundexKey + "00";

                var thirdNumericKeyValue = thirdNumericKey.Value;
                soundexKey += ToString(thirdNumericKeyValue);
                ++_index;

                var fourthNumericKey = MoveToNextCharacterOfInterest(thirdNumericKeyValue);

                if (!fourthNumericKey.HasValue)
                    return soundexKey + "0";
                var fourthNumericKeyValue = fourthNumericKey.Value;
                soundexKey += ToString(fourthNumericKeyValue);
                return soundexKey;
            }

            [ContractInvariantMethod]
            [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Required for code contracts.")]
            private void ObjectInvariant()
            {
                Contract.Invariant(_index > 0);
                Contract.Invariant(_inputLength == _input.Length);
            }

            private int? MoveToNextCharacterOfInterest(int currentNumericKey)
            {
                for (; _index < _inputLength; ++_index)
                {
                    var @char = _input[_index];
                    var upperChar = Char.ToUpperInvariant(@char);
                    if (IsVowel(upperChar))
                    {
                        ++_index;
                        return MoveToFirstIncludedCharacter();
                    }
                    var numericKey = TryGetNumericKey(upperChar);
                    if (!numericKey.HasValue)
                        continue;

                    var numericKeyValue = numericKey.Value;
                    if (numericKeyValue != currentNumericKey)
                        return numericKeyValue;
                }
                return null;
            }

            private int? MoveToFirstIncludedCharacter()
            {
                for (; _index < _inputLength; ++_index)
                {
                    var @char = _input[_index];
                    var upperChar = Char.ToUpperInvariant(@char);
                    var numericKey = TryGetNumericKey(upperChar);
                    if (numericKey.HasValue)
                        return numericKey.Value;
                }
                return null;
            }

            private int? MoveToNextCharacterOfInterest()
            {
                for (; _index < _inputLength; ++_index)
                {
                    var @char = _input[_index];
                    var upperChar = Char.ToUpperInvariant(@char);
                    var numericKey = TryGetNumericKey(upperChar);
                    if (numericKey.HasValue)
                        return numericKey;
                }
                return null;
            }

            private int? MoveToFirstRealCharacterOfInterest(int? firstNumericKey)
            {
                Contract.Requires(_index >= 0);

                if (!firstNumericKey.HasValue)
                    return MoveToNextCharacterOfInterest();

                // If the first character has a numeric key, skip over all subsequent characters which are ignored or have the same numeric key
                var firstNumericKeyValue = firstNumericKey.Value;
                for (; _index < _inputLength; ++_index)
                {
                    var @char = _input[_index];
                    var upperChar = Char.ToUpperInvariant(@char);
                    if (IsIgnoredCharacter(upperChar))
                        // Ignored characters can just be ignored
                        continue;

                    if (IsVowel(upperChar))
                    {
                        // Vowels mean all of the possible characters have been skipped, move to the next index and start the rest of the processing
                        ++_index;
                        return MoveToNextCharacterOfInterest(firstNumericKeyValue);
                    }
                    var secondKey = GetNumericKey(upperChar);
                    if (secondKey != firstNumericKeyValue)
                        return secondKey;
                }

                return null;
            }

            [Pure]
            private static bool IsIgnoredCharacter(char letter)
            {
                switch (letter)
                {
                    case 'A':
                    case 'E':
                    case 'I':
                    case 'O':
                    case 'U':
                    case 'Y':
                    case 'B':
                    case 'F':
                    case 'P':
                    case 'V':
                    case 'C':
                    case 'G':
                    case 'J':
                    case 'K':
                    case 'Q':
                    case 'S':
                    case 'X':
                    case 'Z':
                    case 'D':
                    case 'T':
                    case 'L':
                    case 'M':
                    case 'N':
                    case 'R':
                        return false;
                    default:
                        return true;
                }
            }

            [Pure]
            private static bool IsVowel(char letter)
            {
                switch (letter)
                {
                    case 'A':
                    case 'E':
                    case 'I':
                    case 'O':
                    case 'U':
                    case 'Y':
                        return true;
                    default:
                        return false;
                }
            }

            [Pure]
            private static int? TryGetNumericKey(char letter)
            {
                switch (letter)
                {
                    case 'B':
                    case 'F':
                    case 'P':
                    case 'V':
                        return 1;
                    case 'C':
                    case 'G':
                    case 'J':
                    case 'K':
                    case 'Q':
                    case 'S':
                    case 'X':
                    case 'Z':
                        return 2;
                    case 'D':
                    case 'T':
                        return 3;
                    case 'L':
                        return 4;
                    case 'M':
                    case 'N':
                        return 5;
                    case 'R':
                        return 6;
                    default:
                        return null;
                }
            }

            [Pure]
            private static int GetNumericKey(char letter)
            {
                Contract.Requires(!IsIgnoredCharacter(letter));
                Contract.Requires(!IsVowel(letter));

                var attempt = TryGetNumericKey(letter);
                if (!attempt.HasValue)
                    throw new ArgumentOutOfRangeException("letter");

                return attempt.Value;
            }

            private static char ToCharacter(int value)
            {
                switch (value)
                {
                    case 0:
                        return '0';
                    case 1:
                        return '1';
                    case 2:
                        return '2';
                    case 3:
                        return '3';
                    case 4:
                        return '4';
                    case 5:
                        return '5';
                    case 6:
                        return '6';
                    default:
                        throw new ArgumentOutOfRangeException("value");
                }
            }

            private static string ToString(int value)
            {
                Contract.Ensures(Contract.Result<string>() != null);
                Contract.Ensures(Contract.Result<string>().Length == 1);

                var @char = ToCharacter(value);
                return new string(@char, 1);
            }
        }

        #endregion
    }
}
