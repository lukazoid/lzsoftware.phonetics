using System.Reflection;
using System.Resources;

[assembly: AssemblyCompany("LzSoftware")]
[assembly: AssemblyProduct("LzSoftware.phonetics")]
[assembly: NeutralResourcesLanguage("en-GB")]
[assembly: AssemblyCopyright("Copyright �  2013")]
[assembly: AssemblyVersion("0.1.*")]

#if DEBUG

[assembly: AssemblyConfiguration("Debug")]
#else

[assembly: AssemblyConfiguration("Release")]
#endif